import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FeatureServiceTest {
    @Test
    public void testExistingFeature() throws Exception {
        FeatureService featureService = new FeatureService();

        Feature feature = featureService.getFeature("/dkm/production/feature1");
        assertEquals(feature.getName(), "/dkm/production/feature1");
        assertEquals(feature.isEnabled(), false);
    }

    @Test
    public void testCaching() throws Exception {
        FeatureService featureService = new FeatureService();
        featureService.clearCache();

        Feature feature = featureService.getFeature("/dkm/production/feature1");
        assertEquals(feature.isFromCache(), false);

        feature = featureService.getFeature("/dkm/production/feature1");
        assertEquals(feature.isFromCache(), true);
    }
}
