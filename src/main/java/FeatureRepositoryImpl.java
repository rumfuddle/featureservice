import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FeatureRepositoryImpl implements FeatureRepository {
    private Map<String, Feature> featureMap;
    private final static FeatureRepositoryImpl instance = new FeatureRepositoryImpl();

    private FeatureRepositoryImpl() {
       featureMap = this.parseJson().stream().collect(Collectors.toMap(Feature::getName, f -> f));
    }

    public static FeatureRepositoryImpl getInstance() {
        return instance;
    }

    private List<Feature> parseJson() {
        Gson gson = new Gson();
        Feature[] features = gson.fromJson(new InputStreamReader(getClass().getResourceAsStream("features.json")), Feature[].class);
        return Arrays.asList(features);
    }

    public Feature getFeature(String name) throws UnknownFeatureException {
        if (featureMap.containsKey(name)) {
            Feature feature = featureMap.get(name);
            feature.setFromCache(false);
            return feature;
        }
        else
            throw new UnknownFeatureException("feature " + name + " unknown");
    }
}
