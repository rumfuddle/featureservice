import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class SSMFeatureRepository implements FeatureRepository {
    private static final Logger logger = LoggerFactory.getLogger(SSMFeatureRepository.class);
    private static final String SSMLambdaUri = System.getenv("SSM_LAMBDA_URI");
    private static final SSMFeatureRepository instance = new SSMFeatureRepository();

    public static final int MAX_RETRIES = 1;


    private SSMFeatureRepository() {
        logger.debug("initializing SSMFeatureRepository");
        logger.debug("SSM_LAMBDA_URI is set to {}", SSMLambdaUri);
    }

    public static SSMFeatureRepository getInstance() throws Exception {
        if (SSMLambdaUri == null)
            throw new Exception("SSM_LAMBDA_URI is not defined");
        return instance;
    }

    public Feature getFeature(String name) throws UnknownFeatureException,
            InvalidFeatureException,
            FailedRequestException {
        logger.debug("fetching feature '{}'", name);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            URI uri = new URIBuilder(SSMLambdaUri).addParameter("name", name).build();

            int nr_retries = 0;
            while (nr_retries < MAX_RETRIES) {
                if (nr_retries > 0)
                    logger.debug("going for retry");

                logger.debug("calling {}", uri.toString());
                HttpGet httpGet = new HttpGet(uri);
                CloseableHttpResponse response = httpClient.execute(httpGet);

                int status = getStatusCode(response);
                logger.debug("received status code: {}", status);

                String body = getBody(response);
                logger.debug("received body: {}", body);

                response.close();

                if (status == 200) {
                    Feature feature = parseEntry(body);
                    logger.debug("extracted feature: {}", feature.toString());
                    return feature;
                } else if (status == 404) {
                    throw new UnknownFeatureException("feature " + name + " unknown");
                } else if (status == 504) { //gateway time-out
                    nr_retries++;
                } else
                    throw new FailedRequestException(body);
            }

            throw new FailedRequestException("MAX_RETRIES(" + MAX_RETRIES + ") reached, API gateway time-out");
        } catch (URISyntaxException e) {
            logger.error("URI syntax error: ", e);
           throw new FailedRequestException("URI syntax error: " + e.getMessage());
        } catch (ClientProtocolException e) {
            throw new FailedRequestException("HTTP protocol error: " + e.getMessage());
        } catch (IOException e){
            throw new FailedRequestException("IO error: " + e.getMessage());
        }
    }

    private int getStatusCode(CloseableHttpResponse response) {
        return response.getStatusLine().getStatusCode();
    }

    private String getBody(CloseableHttpResponse response) throws IOException {
        HttpEntity entity = response.getEntity();

        if (entity == null)
            return null;

        InputStream inputStream = entity.getContent();
        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader
                (inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        }

        return textBuilder.toString();
    }

    private Feature parseEntry(String entry) throws InvalidFeatureException {
        Gson gson = new Gson();
        Map<String, Object> map = gson.fromJson(entry, Map.class);
        String name = (String) map.get("parameterName");
        String value = (String) map.get("parameterValue");
        Double metric = (Double) map.get("metric");

        boolean isEnabled = false;
        if (value.equals("true"))
            isEnabled = true;
        else if (value.equals("false"))
            isEnabled = false;
        else
            throw new InvalidFeatureException(String.format("parameter '%s' has value '%s'", name, value));

        Feature feature = new Feature();
        feature.setName(name);
        feature.setEnabled(isEnabled);

        return feature;
    }
}
