public class FailedRequestException extends RuntimeException {
    public FailedRequestException(String message) {
        super(message);
    }
}
