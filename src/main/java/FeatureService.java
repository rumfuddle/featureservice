public class FeatureService {
    private FeatureRepository featureRepository;
    private FeatureCache featureCache;

    public FeatureService() throws Exception {
        featureRepository = SSMFeatureRepository.getInstance();
        featureCache = FeatureCacheImpl.getInstance();
    }

    public Feature getFeature(String name) throws UnknownFeatureException {
        Feature feature = featureCache.getFeature(name);
        if (feature == null) {
            feature = featureRepository.getFeature(name);
            featureCache.storeFeature(feature);
        }

        return feature;
    }

    public void clearCache() {
        featureCache.clear();
    }
}
