public class UnknownFeatureException extends RuntimeException {
    public UnknownFeatureException(String message) {
        super(message);
    }
}
