public class InvalidFeatureException extends RuntimeException {
    public InvalidFeatureException(String message) {
        super(message);
    }
}
