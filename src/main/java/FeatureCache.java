public interface FeatureCache {
    Feature getFeature(String name);

    void storeFeature(Feature feature);

    void clear();
}
