import java.time.LocalDateTime;
import java.util.*;

public class FeatureCacheImpl implements FeatureCache {
    private Map<String, CachedFeature> cache;
    private final static FeatureCacheImpl instance = new FeatureCacheImpl();

    private static int CACHE_EXPIRY_IN_SECONDS = 60;

    private FeatureCacheImpl() {
        cache = new HashMap<String, CachedFeature>();
    }

    public static FeatureCacheImpl getInstance() {
        return instance;
    }

    public Feature getFeature(String name) {
        if (cache.containsKey(name)) {
            CachedFeature cachedFeature = cache.get(name);

            if (isExpired(cachedFeature)) {
                evictFromCache(cachedFeature);
                return null;
            }

            Feature feature = cachedFeature.feature;
            feature.setFromCache(true);
            return feature;
        }

        return null;
    }

    public void storeFeature(Feature feature) {
        CachedFeature cachedFeature = new CachedFeature();
        cachedFeature.feature = feature;
        cachedFeature.expiresAt = LocalDateTime.now().plusSeconds(CACHE_EXPIRY_IN_SECONDS);
        cache.put(feature.getName(), cachedFeature);
    }

    private Boolean isExpired(CachedFeature cachedFeature) {
       return cachedFeature.expiresAt.isBefore(LocalDateTime.now());
    }

    private void evictFromCache(CachedFeature cachedFeature) {
        cache.remove(cachedFeature.feature.getName());
    }

    public void clear() {
        cache.clear();
    }

    private class CachedFeature {
        private Feature feature;
        private LocalDateTime expiresAt;
    }
}
