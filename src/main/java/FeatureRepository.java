public interface FeatureRepository {
    Feature getFeature(String name) throws UnknownFeatureException;
}
